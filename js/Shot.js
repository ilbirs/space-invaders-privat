function createShot(x,y ,speed, monsterHit, onDestroyed) {
    const svg = document.getElementById("svg1");
    const line = document.createElementNS("http://www.w3.org/2000/svg", "line");

    line.setAttribute('x1', ""+x);
    line.setAttribute('y1', ""+y);
    line.setAttribute('x2', ""+x);
    line.setAttribute('y2', ""+y);

    line.setAttribute("style","stroke:orange;stroke-width:2");

    svg.appendChild(line);

    return {
        x: x,
        y: y,
        line: line,
        ref: undefined,
        auto: function() {
            this.ref = setInterval(() => this.moveUp(), 25);
        },

        remove: function() {
            if (this.ref) {
                clearInterval(this.ref);
                this.ref = undefined;
            }
            if (this.line) {
                svg.removeChild(this.line);
                this.line = undefined;
                onDestroyed();
            }
        },


        moveUp: function() {
            if (monsterHit (this.x, this.y-30 )) {
                this.remove()
                return;
            }
            if (this.y <= 50 || this.y >= 890) {
                this.remove();
                return;
            }
            this.y -= speed;
            this.line.setAttribute('y1', ""+(this.y-30));
            this.line.setAttribute('y2', ""+this.y);
        },
    }
}
