
function createMonster(x,y, onDestroy) {
    const svg = document.getElementById("svg1");
    let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
    circle.setAttribute("style", "fill:red; stroke:black; stroke-width:2");
    circle.setAttribute("cx", "" + x);
    circle.setAttribute("cy", "" + y);
    circle.setAttribute("r", "20");
    svg.appendChild(circle);

    return {
        x: x,
        y: y,
        circle: circle,
        direction: "RIGHT",
        movementInterval: undefined,
        /*   moveLeft: function() {
               if (this.x <= 50)
                   return;
              // this.move(this.x - 5, this.y);
           },
           moveRight: function() {
               if (this.x >= 750)
                   return;
               //this.move(this.x + 5, this.y);
           },*/
        move: function (x, y) {
            this.x = x;
            this.y = y;
            circle.setAttribute("cx", "" + x);
            circle.setAttribute("cy", "" + y);
        },

        shooooot: function () {
            let shot = createShot(this.x, this.y + 50, -4/*--=+*/, (x, y) => this.spaceShip.ifHitLoose(x, y), () => {
            });
            shot.auto();
        },
        remove: function () {
            if (this.movementInterval) {
                clearInterval(this.movementInterval);
                this.movementInterval = null;
            }
            if (this.circle) {
                svg.removeChild(this.circle);
                this.circle = undefined;
            }
            onDestroy(this);
        },

        stop: function () {
            if (this.movementInterval) {
                clearInterval(this.movementInterval);
                this.movementInterval = null;
            }
        },

        autoMove: function () {
            if (this.y >= 650) {
                this.remove();
                return;
            }
            if (this.direction === "RIGHT") {
                if (this.x >= 850) {
                    this.move(this.x, this.y + 50);
                    this.direction = "LEFT";
                    return;
                }
                this.move(this.x + 2, this.y);
            } else {
                if (this.x <= 50) {
                    this.move(this.x, this.y + 100);
                    this.direction = "RIGHT";
                    return;
                }
                this.move(this.x - 2, this.y);
            }
            let doShoot = Math.floor(Math.random() * 100);
            if (doShoot === 0) {
                this.shooooot();
            }
        },
        activate: function () {
            this.movementInterval = setInterval(() => this.autoMove(), 20);
        },
        ifHitDestroy: function (shotX, shotY) {
            if ((shotY > this.y - 20) && (shotY < this.y + 20)
                && (shotX > this.x - 40) && (shotX < this.x + 40)) {
                this.remove();
                return true;
            }
            return false;

        }
    }
}