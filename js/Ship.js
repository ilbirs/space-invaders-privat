function createSpaceShip(x,y) {
    const svg = document.getElementById("svg1");
    const polygon = document.createElementNS("http://www.w3.org/2000/svg", "polygon");
    polygon.setAttribute("style","fill:blue; stroke:red; stroke-width:2");
    svg.appendChild(polygon);

    let points = arr = [
        [ x,y ],
        [ x+100,y=630 ],
        [ x+50,y-100 ]]
        .map(point => ""+point[0]+","+point[1])
        .join(" ");

    console.log("points",points);

    polygon.setAttribute('points', points);

    return {
        x: x,
        y: y,
        lives:3,
        polygon: polygon,
        countShots: 0,
        moveLeft: function() {
            if (this.x <= 20)
                return;
            this.move(this.x - 5, this.y);
        },
        moveRight: function() {
            if (this.x >= 800)
                return;
            this.move(this.x + 5, this.y);
        },
        move: function(x,y) {
            this.x = x;
            this.y = y;
            let points = arr = [
                [ x,y ],
                [ x+100,y ],
                [ x+50,y-100 ]]
                .map(point => ""+point[0]+","+point[1])
                .join(" ");
            this.polygon.setAttribute('points', points);
        },
        shooooot: function () {
            if (this.countShots >=3)
                return;
            this.countShots++;
            let shot = createShot(this.x+50, this.y - 100 ,/*i filen shot satt vi speed på y-=speed = -+=-*/ 20, monsterHit, () => this.countShots--);
            shot.auto();
        },
        ifHitLoose: function (shotX,shotY) {
            /*  if (this.lives===0)
                  return false;*/
            if ( (shotY > this.y - 100) && (shotY < this.y)
                && (shotX > this.x) && (shotX < this.x + 100) ) {
                this.lives--;
                document.getElementById("lives").innerHTML=""+this.lives;
                if (this.lives===0)
                    onLoose();
                return true;
            }
            return false;
        }

    }
}
