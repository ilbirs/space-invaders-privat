var monsters = [100,200,300,400,500]
    .map(x => createMonster(x,100, destroyedMonster => monsters = monsters.filter(/*behåller allting i listan som ger true*/monster => monster !== destroyedMonster)));
monsters.forEach(monster => monster.activate());

/* skapar nya monsters värje sekund random 0 av 5*/
const createMonstersInterval = setInterval(() => {
    let number = Math.floor(Math.random() * 5);
    if (number === 0) {
        let monster = createMonster(100,100, destroyedMonster => monsters = monsters.filter(monster => monster !== destroyedMonster));
        monsters.push(monster);
        monster.spaceShip = spaceShip
        monster.activate();
    }
}, 500);

const monsterHit = function (x,y) {
    return monsters.map(monster => monster.ifHitDestroy(x,y))
        .filter(hit => hit)
        .length > 0;
};



const onLoose = function () {
    clearInterval(createMonstersInterval);
    clearInterval(checkWinInterval);
    monsters.forEach(monster => monster.stop());
    alert("You loose");
};

const checkWin = function () {
    if (monsters.length === 0) {
        clearInterval(createMonstersInterval);
        clearInterval(checkWinInterval);
        alert("You win");
    }
};


const spaceShip = createSpaceShip(100,630, monsterHit, onLoose);



/* samma konstruktion fast med twå rader monster
const monster = [
 [100,100],[220,100],[330,100],[440,100],[550,100],
 [100,140],[220,140],[330,140],[440,140],[550,140],
 .map(xy => createMonster(xy[0], xy[1]));
 */

const checkWinInterval = setInterval(() => checkWin(), 1000);



monsters.forEach(monster => monster.spaceShip = spaceShip)

document.addEventListener('DOMContentLoaded', () => {
    'use strict';

    document.addEventListener('keydown', e => {
        //console.log("event",e.key);
        if (e.key === 'ArrowLeft')
            spaceShip.moveLeft();
        if (e.key === 'ArrowRight')
            spaceShip.moveRight();
        if (e.key === ' ')
            spaceShip.shooooot();
    });
});



